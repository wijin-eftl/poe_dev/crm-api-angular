import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { Order } from 'src/app/core/models/order';
import { OrdersService } from '../../orderService/orders.services';

@Component({
  selector: 'app-page-edit-order',
  templateUrl: './page-edit-order.component.html',
  styleUrls: ['./page-edit-order.component.scss']
})
export class PageEditOrderComponent implements OnInit {

  public order_id: number = 0;
  public order: Order = {} as Order
  public orderId : number = 0;


  public success = false;
  public failure = false;
  public formulaireOrderEdit!: FormGroup;
  private souscription: Subscription | null = null;

  constructor(
      private activatedRoute: ActivatedRoute,
      private ordersService: OrdersService,
      private router: Router,
      private formBuilder:  FormBuilder)  { }

  getClientId() {
    return this.order_id;

  }

  initForm(order: Order): void {
    this.formulaireOrderEdit = this.formBuilder.group({
    numberOfDays: [
    '',
    [Validators.required, Validators.pattern(/^-?(0|[1-9]\d*)?$/)],
    ],
    tva: ['', [Validators.required]],
    status: ['', [Validators.required]],
    type: ['', [Validators.required]],
    notes: ['', [Validators.maxLength(254)]],
  });

  }

  ngOnInit(): void {

    // on récupère la dernière partie de l'URL pour obtenir le clientId, seule l'activatedRoute le permet !
    this.activatedRoute.url.subscribe(
      url => {

        this.order_id = Number(url[url.length -1])
    // On recherche le Client
        this.ordersService.getOrderById(this.order_id).subscribe(
          order => {
            this.order = order;
            this.orderId = this.order.id;
            this.initForm(order);
          } // permet d'afficher le Titre de la page Edit avec le nom du Client
        );
      }
    );
  }

  ngOnDestroy(): void {
    // Destruction de l'observateur
    if (this.souscription) {
      this.souscription.unsubscribe();
    }
  }

  goBackToList() {
    // navigateByUrl n'utilise que des chemins absolus
    // this.router.navigateByUrl('/clients/list');
    this.router.navigate( ['../../list'], {relativeTo: this.activatedRoute});
  }

  updateOrder() {

    this.ordersService.updateOrder(this.order).subscribe(
      {

        next: () => { this.success = true; },
        error: () => { this.failure = true; }
      }

    );

  }
}
