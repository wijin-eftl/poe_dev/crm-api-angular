import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Customer } from 'src/app/core/models/customer';
import { Order } from 'src/app/core/models/order';
import { CustomerService } from 'src/app/customers/services/customer.service';
import { OrdersService } from '../../orderService/orders.services';

//LES METHODES NE SONT PAS ENCORE AFFINEES CETTE VERSION EST EVOLUTIVE

@Component({
  selector: 'app-page-add-order',
  templateUrl: './page-add-order.component.html',
  styleUrls: ['./page-add-order.component.scss'],
})
export class PageAddOrderComponent implements OnInit {
  public success = false;
  public failure = false;
  private souscription: Subscription | null = null;

  public clients: Customer[]|null=null;


  public formulaireOrder!: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private OrdersService: OrdersService,
    private customersService: CustomerService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
  ) {}

  public ClientId!:number;

  public customerDtolId!: Customer;


  ngOnInit(): void {

    this.formulaireOrder = this.formBuilder.group({

      customerId: ['', [Validators.required]],

      label: [null, [Validators.required, Validators.maxLength(1000)]],
      adrEt: [
        null,
        [Validators.required, Validators.pattern(/^-?(0|[1-9]\d*)?$/)],
      ],
      numberOfDays: [
        null,
        [Validators.required, Validators.pattern(/^-?(0|[1-9]\d*)?$/)],
      ],
      tva: [null, [Validators.required]],
      status: [null, [Validators.required]],
      type: [null, [Validators.required]],
      notes: ['', [Validators.maxLength(254)]],

    });

    this.customersService.getAllCustomers().subscribe(data=>{
      this.clients=data;})

  }

  ngOnDestroy(): void {
    // Destruction de l'observateur
    if (this.souscription) {
      this.souscription.unsubscribe();
    }
  }

  public  orderAdd!: Order;
  addCreatedOrder() {
    this.customerDtolId = new Customer({});
    this.customerDtolId.id = this.formulaireOrder.value.customerId
    this.orderAdd = this.formulaireOrder.value
    this.orderAdd.customerDto = this.customerDtolId
    this.OrdersService.addOrder(this.orderAdd).subscribe({
      next: () => {
        this.success = true;
        this.goBackToList()
      },
      error: () => {
        this.failure = true;
      },

    });
    this.customerDtolId.id = this.formulaireOrder.value.customerId
  }

  goBackToList() {

    this.router.navigate(['/orders/list'], { relativeTo: this.activatedRoute });
  }


}
