import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { map, Observable, Subscription, tap } from 'rxjs';
import { AuthService } from 'src/app/auth/service/auth.service';
import { Customer } from 'src/app/core/models/customer';
import { Order } from 'src/app/core/models/order';
import { Role } from 'src/app/core/models/roles';
import { OrdersService } from '../../orderService/orders.services';

@Component({
  selector: 'app-page-list-order',
  templateUrl: './page-list-order.component.html',
  styleUrls: ['./page-list-order.component.scss']
})
export class PageListOrderComponent implements OnInit {

  public success=false;
  public failure=false;
  public orders$!: Observable<Order[]>;
  public customers$! : Observable<Customer[]>;
  public deleteClientID:number=0;
  private souscription: Subscription | null = null;
  page = 1;
  pageSize =10;
  public total=0;
  public isAdmin=false;

  constructor(private ordersService: OrdersService, private modalService: NgbModal,
    private authService:AuthService) { }

  ngOnInit(): void {

    this.isAdmin=Role.Admin===this.authService.getUserLogGrants();
    this.getOrders();
  }

  ngOnDestroy(): void {
    // Destruction de l'observateur
    if (this.souscription) {
      this.souscription.unsubscribe();
    }
  }

  getOrders() {
    this.orders$ = this.ordersService.getAllOrders().pipe(
    tap( orders => this.total = orders.length)
    );
    this.orders$.forEach(element => {
    });
  }

  paidOrders(){
    this.orders$ = this.ordersService.getOrderByStatus().pipe(
      tap( data => this.total = data.length)
    );

  }


  deleteOrder(orderId: number) {
    this.ordersService.deleteOrder(orderId).subscribe(
      { error: () => {
        this.failure = true;
      },
        complete: () => {
        this.getOrders();
        this.success = true;
      }}
    );

  }

  confirmDeletion(content: any, orderId: number) {
    this.modalService.open(content,
      {ariaLabelledBy: 'modal-basic-title'})
      .result.then((result) => {
        if (result) {
          this.deleteOrder(orderId);

        }
    }, (reason) => {

    });
  }

}
