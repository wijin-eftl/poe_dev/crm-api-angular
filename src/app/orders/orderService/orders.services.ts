import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Order} from 'src/app/core/models/order';
import {AuthService} from "../../auth/service/auth.service";

@Injectable({
  providedIn: 'root',
})
export class OrdersService {

  private url = '/api';

  constructor(private httpOrder: HttpClient,
              private authService : AuthService) {
  }

  getAllOrders(): Observable<Order[]> {
    return this.httpOrder.get<Order[]>(`${this.url}/orders`, {headers: this.authService.getHeaders()});
  }

  getOrderById(orderId: number): Observable<Order> {
    return this.httpOrder.get<Order>(`${this.url}/orders/${orderId}`, {headers: this.authService.getHeaders()});
  }

  getOrderBycustomerId(customerId: number): Observable<Order[]> {
    return this.httpOrder.get<Order[]>(
      `${this.url}/orders/customers/${customerId}`, {headers: this.authService.getHeaders()}
    );
  }

  getOrderByStatus(orderStatus: String = "Payée"): Observable<Order[]> {
    return this.httpOrder.get<Order[]>(`${this.url}/orders?status=${orderStatus}`,{headers: this.authService.getHeaders()});
  }

  addOrder(order: Order): Observable<void> {
    return this.httpOrder.post<void>(`${this.url}/orders`, order, {headers: this.authService.getHeaders()});
  }

  deleteOrder(orderId: number): Observable<void> {
    return this.httpOrder.delete<void>(`${this.url}/orders/${orderId}`, {headers: this.authService.getHeaders()});
  }

  updateOrder(order: Order): Observable<void> {
    return this.httpOrder.put<void>(`${this.url}/orders/${order.id}`, order, {headers: this.authService.getHeaders()});
  }
}
