import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrdersRoutingModule } from './orders-routing.module';
import { PageListOrderComponent } from './pages/page-list-order/page-list-order.component';
import { PageAddOrderComponent } from './pages/page-add-order/page-add-order.component';
import { PageEditOrderComponent } from './pages/page-edit-order/page-edit-order.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { TemplatesModule } from '../templates/templates.module';
import { SharedModule } from '../shared/shared.module';

import { DescrOrderComponent } from './pages/descr-order/descr-order.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {IconsModule} from "../icons/icons.module";



@NgModule({
  declarations: [
    PageListOrderComponent,
    PageAddOrderComponent,
    PageEditOrderComponent,
    DescrOrderComponent
  ],
  imports: [
    CommonModule,
    OrdersRoutingModule,
    FormsModule,
    TemplatesModule,
    SharedModule,
    ReactiveFormsModule,
    NgbModule,
    IconsModule
  ],
  exports: [
    PageListOrderComponent,
    PageEditOrderComponent,
    PageAddOrderComponent,
    DescrOrderComponent
  ]
})

export class OrdersModule { }
