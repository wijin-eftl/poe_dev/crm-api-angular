import {Injectable} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {Observable} from "rxjs";
import {User} from "../../core/models/user";
import {HttpClient, HttpHeaders} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(    private router: Router,
                  private activatedRoute: ActivatedRoute,
                  private httpClient: HttpClient) {
  }

  private token!: string;
  private _username !: string;
  private _password !: string;
  private _grants !: string;
  private _headers !: HttpHeaders;

  getHeaders(): HttpHeaders {
    return this._headers;
  }

  getUserLogGrants(): string {
    return this._grants;
  }

  setGrants(value: string) {
    this._grants = value;
  }

  getUserByUsernameAndPassword(username: string, password: string): Observable<User> {
    return this.httpClient.get<User>(`/api/users/login?username=${username}&password=${password}`);
  }

  login(username: string, password: string) {
    this._headers = new HttpHeaders({authorization: 'Basic ' + btoa(username + ':' + password)});
    this.getUserByUsernameAndPassword(username,password).subscribe({

      // on contrôle maintenant si le username et le password correspondent au back...
      next: (user) => {
        this.setGrants(user.grants);
        this.setToken('monRole' + user.grants);
        this.router.navigate(['../home'], {relativeTo: this.activatedRoute})
      }
    });

  }

  getToken(): string {
    return this.token;
  }

  setToken(monToken: string) {
    this.token = monToken;
  }

  logout() {
    this.setToken('');
    this._headers=new HttpHeaders() ;
  }
}
