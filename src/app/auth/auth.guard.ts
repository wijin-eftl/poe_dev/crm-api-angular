import {Injectable} from '@angular/core';
import {AuthService} from "./service/auth.service";
import {CanActivate, Router} from "@angular/router";


@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private auth: AuthService,
              private router: Router) {
  }

  canActivate(): boolean {
    const token = this.auth.getToken();
    if (token) {
      return true;
    } else {
      this.router.navigateByUrl('/login');
      return false;
    }
  }


  isConnected(): boolean {
    const token = this.auth.getToken();
    if (token) {
      return true;
    } else {
      return false;
    }

  }
}
