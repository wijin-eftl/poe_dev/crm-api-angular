import {Component, OnInit} from '@angular/core';
import {User} from "../../../core/models/user";
import {Subscription} from "rxjs";
import {ActivatedRoute} from "@angular/router";
import {UsersService} from '../../services/users.service/users.service';

@Component({
  selector: 'app-descr-user',
  templateUrl: './descr-user.component.html',
  styleUrls: ['./descr-user.component.scss']
})

export class DescrUserComponent implements OnInit {

  public user!: User;
  public userid: number = 0;
  private souscriptionGetVersion: Subscription | null = null;

  constructor(
    private activatedRoute: ActivatedRoute,
    private userService: UsersService) {

  }

  ngOnInit(): void {
    // on récupère la dernière partie de l'URL pour obtenir le userId
    this.activatedRoute.url.subscribe(
      url => {
        this.userid = Number(url[url.length - 1]);

        // On recherche l'utilisateur
        this.userService.getUserById(this.userid).subscribe(
          (user: User) => {
            this.user = user;
          }
        );
      }
    );
  }

  ngOnDestroy(): void {
    // Destruction de l'observateur
    if (this.souscriptionGetVersion) {
      this.souscriptionGetVersion.unsubscribe();
    }
  }

}

