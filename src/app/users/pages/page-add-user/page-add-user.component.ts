import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from 'src/app/core/models/user';
import { UsersService } from '../../services/users.service/users.service';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {BcryptServiceService} from 'src/app/shared/services/bcrypt-service.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-page-add-user',
  templateUrl: './page-add-user.component.html',
  styleUrls: ['./page-add-user.component.scss']
})
export class PageAddUserComponent implements OnInit {
  nrSelect:string = "USER";
  public formulaire!: FormGroup;
  public success = false;
  public failure = false;
  public setDisable = false;
  public userExist=false;
  public errorMessage="";
  private souscription: Subscription | null = null;

  constructor(
    private usersService:UsersService,
    private router:Router,
    private activatedRoute:ActivatedRoute,
    private formBuilder:FormBuilder,
    private bcryptServiceService: BcryptServiceService
  ) { }

  ngOnInit(): void {

    this.formulaire=this.formBuilder.group({
        username:['', [Validators.minLength(3), Validators.maxLength(25),Validators.required, Validators.pattern('[a-zA-Z0-9]+')]],
        password: ['',[Validators.minLength(5), Validators.maxLength(15) ,Validators.required, Validators.pattern('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])[a-zA-Z0-9]+$')]],//this is for the letters (both uppercase and lowercase) and numbers validation
        mail:['',[Validators.minLength(1), Validators.maxLength(255) ,Validators.required, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]],
        grants:['', [Validators.required]]    });
  }

  ngOnDestroy(): void {
    // Destruction de l'observateur
    if (this.souscription) {
      this.souscription.unsubscribe();
    }
  }

  ajouter(){
    this.formulaire.value.password=this.bcryptServiceService.setBcryptPassword(this.formulaire.value.password);
    this.usersService.addUser(
    this.formulaire.value as User
).subscribe({
  next: () => { this.success = true; this.setDisable = true;},
  error: (error) =>{
    this.errorMessage=error.error.message
    this.failure = true;
}
});
}

goBackToList() {
this.router.navigate(['../list'], {relativeTo: this.activatedRoute})
}

}
