import { TestBed } from '@angular/core/testing';
import { HttpClient } from '@angular/common/http';

import { UsersService } from './users.service';

describe('UsersService', () => {
  let service : UsersService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      providers: [ 
        {provide: HttpClient, useValue: {} },
       ]
    });
    service = TestBed.inject(UsersService);
  });

  it('should create', () => {
    expect(service).toBeTruthy();
  });
});
