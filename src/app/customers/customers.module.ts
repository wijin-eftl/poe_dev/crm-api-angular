import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { CustomersRoutingModule } from './customers-routing.module';
import { PageAddCustomerComponent } from './pages/page-add-customer/page-add-customer.component';
import { PageEditCustomerComponent } from './pages/page-edit-customer/page-edit-customer.component';
import { PageListCustomerComponent } from './pages/page-list-customer/page-list-customer.component';
import { IconsModule } from '../icons/icons.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PageDescrCustomerComponent } from './pages/page-descr-customer/page-descr-customer.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    PageAddCustomerComponent,
    PageEditCustomerComponent,
    PageListCustomerComponent,
    PageDescrCustomerComponent
  ],
  imports: [
    CommonModule,
    CustomersRoutingModule,
    IconsModule,
    FormsModule,
    NgbModule,
    SharedModule
  ],
  exports: [
    PageAddCustomerComponent,
    PageEditCustomerComponent,
    PageListCustomerComponent,
    PageDescrCustomerComponent
  ]
})
export class CustomersModule { }
