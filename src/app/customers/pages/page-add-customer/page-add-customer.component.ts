import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Customer } from 'src/app/core/models/customer';
import { CustomerService } from '../../services/customer.service';

@Component({
  selector: 'app-page-add-customer',
  templateUrl: './page-add-customer.component.html',
  styleUrls: ['./page-add-customer.component.scss']
})
export class PageAddCustomerComponent implements OnInit {

  public formulaire! : FormGroup;

  public success = false;
  public failure = false;
  public setDisable = false;
  private souscription: Subscription | null = null;

  constructor(
    private CustomerService: CustomerService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder

    ) { }

  ngOnInit(): void {
    this.formulaire = this.formBuilder.group({
      // id:[0],
      lastname:['',[Validators.required, Validators.minLength(2), Validators.maxLength(100),Validators.pattern("^[A-Z]{2,100}$")]],
      firstname:['',[Validators.required, Validators.minLength(2), Validators.maxLength(100),Validators.pattern("[a-zA-ZäâéçÉ ]{2,100}")]],
      company:['',[Validators.required, Validators.minLength(2), Validators.maxLength(200)]],
      mail:['',[Validators.required, Validators.minLength(8), Validators.maxLength(255), Validators.pattern("[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}")]],
      phone:['',[Validators.required, Validators.minLength(10), Validators.maxLength(13), Validators.pattern("(0)[1-9][0-9]{8}")]],
      mobile:['',[Validators.required,Validators.pattern("(0)[6-7][0-9]{8}")]],
      notes:['', [Validators.maxLength(255)]],
      active:[false, [Validators.required]]
      });
  }

  ngOnDestroy(): void {
    // Destruction de l'observateur
    if (this.souscription) {
      this.souscription.unsubscribe();
    }
  }

  ajouter() {
    this.CustomerService.addCustomer(
      this.formulaire.value as Customer
      ).subscribe({
        next: () => { this.success = true; this.setDisable = true; },
        error:() => { this.failure = true; }
      });
  }

  goBackToList() {
    this.router.navigate(['../list'], {relativeTo: this.activatedRoute})
  }

}
