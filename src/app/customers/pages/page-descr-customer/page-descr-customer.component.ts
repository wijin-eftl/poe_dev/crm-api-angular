import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable, Subscription, tap } from 'rxjs';
import { Customer } from 'src/app/core/models/customer';
import { Order } from 'src/app/core/models/order';
import { OrdersService } from 'src/app/orders/orderService/orders.services';
import { CustomerService } from '../../services/customer.service';

@Component({
  selector: 'app-page-descr-customer',
  templateUrl: './page-descr-customer.component.html',
  styleUrls: ['./page-descr-customer.component.scss']
})
export class PageDescrCustomerComponent implements OnInit {

  public cust !: Customer ;
  public orders$!: Observable<Order[]>;
  // info commande
  public orderid: number = 0;
  public customer_id: number = 0;
  public total=0;
  page = 1;
  pageSize =5;

  private souscriptionGetVersion: Subscription | null = null;

  constructor(
    private activatedRoute: ActivatedRoute,
    private orderService: OrdersService,
    private customerService: CustomerService
  ) { }

  ngOnInit(): void {

     // on récupère la dernière partie de l'URL pour obtenir le customer Id
   this.activatedRoute.url.subscribe(
     url => {
        this.customer_id = Number(url[url.length - 1]);

       // On recherche le customer
       this.customerService.getCustomerById(this.customer_id).subscribe(
         (customer)=> {
           this.cust = customer

         }
       )
      }
       );
        this.fetchOrders();
     }
  // On recherche les commandes associées
  fetchOrders() {
    this.orders$ = this.orderService.getOrderBycustomerId(this.customer_id).pipe(
    tap( orders => this.total = orders.length)
      );
    this.orders$.forEach(element => {
         });
    }

ngOnDestroy(): void {
   // Destruction de l'observateur
   if (this.souscriptionGetVersion) {
     this.souscriptionGetVersion.unsubscribe();
   }
 }
}
