import {CustomersI} from "../interfaces/customers-i";

export class Customer implements CustomersI {
  id: number = 0;
  lastname: string = '';
  firstname: string = '';
  company: string = '';
  mail: string = '';
  phone: string = '';
  mobile: string = '';
  active: boolean = false;
  notes: string = '';

  constructor(partialCustomer: Partial<Customer>) {
    if (partialCustomer) {
      Object.assign(this, partialCustomer);
    }
  }



}
