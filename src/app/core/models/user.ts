import {UsersI} from "../interfaces/users-i";
import { Role } from "./roles";

export class User implements UsersI {
  id: number = 0;
  username: string = '';
  password: string = '';
  mail : string = '';
  grants: Role = Role.User

  constructor(partialUser: Partial<User>) {
    if (partialUser) {
      Object.assign(this, partialUser);
    }
  }

}
