import { OrdersI } from "../interfaces/orders-i";
import { Customer } from "./customer";

export class Order implements OrdersI {

  id: number = 0;
  customer_id: number = 0;
  customerDto!: Customer;
  label: string = '';
  numberOfDays: number = 0;
  adrEt: number = 0;
  tva: number = 0;
  status: string = '';
  type: string = '';
  notes: string = '';


constructor(partialOrder: Partial<Order>) {
  if (partialOrder) {
    Object.assign(this, partialOrder);
  }

  }




}
