import { Component, OnInit } from '@angular/core';
import {AuthService} from "../../../auth/service/auth.service";
import {AuthGuard} from "../../../auth/auth.guard";
import {Router} from "@angular/router";


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  isConnected!:boolean;

  constructor(
    private auth: AuthService,
    private authService:AuthService,
    private authGuard:AuthGuard,
    private router:Router
  ) { }

  ngOnInit(): void {
    this.isConnected = this.authGuard.isConnected();
  }

  logout(){
    if(this.authGuard.isConnected()){
      this.authService.logout();
      this.router.navigateByUrl('login');
    }
  }

  Connection(): boolean {
    const token = this.auth.getToken();
    if (token) {
      return true;
    } else {
      return false;
    }

  }



}
