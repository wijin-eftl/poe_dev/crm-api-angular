import { Component, OnInit } from '@angular/core';
import {AuthService} from "../../../auth/service/auth.service";

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})

export class NavComponent implements OnInit {


  constructor(private authService:AuthService){}

  ngOnInit(): void {
  }

  isAdmin(): boolean {
    if (this.authService.getUserLogGrants()=="ADMIN") {
      return true
    } else {
      return false
    }
  }



}
