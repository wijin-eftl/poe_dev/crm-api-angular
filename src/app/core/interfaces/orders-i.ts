
export interface OrdersI {
  id: number;
  customer_id:number;
  label: string;
  adrEt: number;
  numberOfDays: number;
  tva: number;
  status: string;
  type: string;
  notes: string;
}
